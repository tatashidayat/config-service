package com.th.rnd.customerservice.repo;

import java.util.Optional;
import com.th.rnd.customerservice.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<Customer, Long>{
    Optional<Customer> findByUsername(String username);
}