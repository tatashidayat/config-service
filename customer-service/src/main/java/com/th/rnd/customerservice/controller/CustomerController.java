package com.th.rnd.customerservice.controller;

import java.util.Optional;

import com.th.rnd.customerservice.entity.Customer;
import com.th.rnd.customerservice.repo.CustomerRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
public class CustomerController{

    private CustomerRepo customerRepo;

    @Autowired
    CustomerController(CustomerRepo customerRepo){
        this.customerRepo = customerRepo;
    }

    @RequestMapping(value="/customers", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Customer>> readCustomer() {
        return new ResponseEntity<Iterable<Customer>> ((Iterable<Customer>)customerRepo.findAll(),HttpStatus.OK);
    }
    
    @RequestMapping(value="/customers/{username}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> readCustomerByUsername(@PathVariable String username){
        Optional<Customer> cekCust = customerRepo.findByUsername(username);
        if (cekCust.isPresent()){
            return new ResponseEntity<Customer>(cekCust.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
