package com.th.rnd.customerservice;

import java.sql.Timestamp;
import java.util.Arrays;

import com.th.rnd.customerservice.entity.Customer;
import com.th.rnd.customerservice.repo.CustomerRepo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@EnableDiscoveryClient
@SpringBootApplication
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner clr(CustomerRepo customerRepo) {
		return (tes) -> Arrays.asList("nadira,ilham,guna,puji,luthfi,ulwi,tatas".split(",")).forEach(a -> {
			Customer customer = new Customer("username " + a, a, a + "@emeriocorp.com", "alamat " + a,
					new Timestamp(System.currentTimeMillis()));
			customerRepo.save(customer);
		});
	}
}
