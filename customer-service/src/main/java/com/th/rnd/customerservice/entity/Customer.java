package com.th.rnd.customerservice.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Customer{
    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String name;
    private String email;
    private String address;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Timestamp dateofbirth;

    @Lob
    @Column(name="customerimage", nullable=true, columnDefinition="mediumblob")
    private Byte[] customerimage;

    @SuppressWarnings("unused")
    private Customer(){}

    public Customer(final String username){
        this.setUsername(username);
    }

    public Customer(final String username, final String name, final String email, final String address, final Timestamp dateofbirth){
        this.setUsername(username);
        this.setName(name);
        this.setEmail(email);
        this.setAddress(address);
        this.setDateofbirth(dateofbirth);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getDateofbirth() {
        return this.dateofbirth;
    }

    public void setDateofbirth(Timestamp dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public Byte[] getCustomerimage() {
        return this.customerimage;
    }

    public void setCustomerimage(Byte[] customerimage) {
        this.customerimage = customerimage;
    }
}