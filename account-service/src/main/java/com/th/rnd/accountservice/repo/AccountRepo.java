package com.th.rnd.accountservice.repo;

import java.util.Optional;

import com.th.rnd.accountservice.entity.Account;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepo extends CrudRepository<Account, Long>{
    Iterable<Account> findByNama(String nama);
    Optional<Account> findById(Long id);
}