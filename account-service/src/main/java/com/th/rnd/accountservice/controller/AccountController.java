package com.th.rnd.accountservice.controller;

import java.util.Optional;

import com.th.rnd.accountservice.client.CustomerServiceClient;
import com.th.rnd.accountservice.entity.Account;
import com.th.rnd.accountservice.repo.AccountRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class AccountController{

    private final AccountRepo accountRepo;
    
    @Autowired
    private AccountController(AccountRepo accountRepo){
        this.accountRepo = accountRepo;
    }

    @Autowired
    private CustomerServiceClient customerServiceClient;

    @RequestMapping(value="/accounts", method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Account>> getAllAccounts() {
        return new ResponseEntity<>((Iterable<Account>)accountRepo.findAll(), HttpStatus.OK);
    }

    //get by nama
    @RequestMapping(value="/accounts/{nama}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Account>> getAccountByNama(@PathVariable String nama) {
        Iterable<Account> cekAccount = accountRepo.findByNama(nama);
        if (cekAccount.iterator().hasNext()){
            return new ResponseEntity<>(accountRepo.findByNama(nama),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // create account
    @RequestMapping(value="/accounts", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> tambahAccount(@RequestBody Account acc) {
        Object cek=customerServiceClient.getCustomer(acc.getNama());
        if (cek!=null){
            System.out.println("\n\n\nusername ditemukan\n\n\n");
            Account newAccount = accountRepo.save(acc);
            return new ResponseEntity<>(newAccount,HttpStatus.CREATED);
        }
        System.out.println("\n\n\nusername tidak ditemukan\n\n\n");
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    // update account
    @RequestMapping(value="/accounts/{id}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> editAccount(@PathVariable Long id, @RequestBody Account acc) {
        Optional<Account> cek = accountRepo.findById(id);
        if (cek.isPresent()){
            Account newAcc = accountRepo.save(acc);
            return new ResponseEntity<>(newAcc, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // delete account
    @RequestMapping(value="/accounts/{id}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> requestMethodName(@PathVariable Long id) {
        Optional<Account> cek = accountRepo.findById(id);
        if (cek.isPresent()){
            this.accountRepo.delete(cek.get());
            return new ResponseEntity<>(cek.get(),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}