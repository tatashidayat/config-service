# Microservice vs Monolithic

# Netflix OSS
Netflix OSS adalah suatu kumpulan framework dan library yang sangat berfungsi untuk microservice. Netflix memutuskan untuk bergabung ke dunia Open Source. Netflix menyediakan dan mengembangkan teknologi open source di bidang Internet Television Network. Teknologi depolyment netflix memungkinkan untuk aktivitas CI/CD.
Berikut beberapa teknologi open source yang dimiliki oleh Netflix :
1. Big Data
2. Build and Delivery Tools
3. Common Runtime Service & Library
4. Content Encoding
5. Data Persistance
6. Insight, Reliability and Performance
7. Security
8. User Interface


# Microservice with Netflix OSS
Microservice ini dibuat menggunakan Spring Boot dan Netflix OSS. Berikut beberapa service dan dependencies yang akan dibuat :

1. Config Service
```
group		: com.emerio.rnd
artifact	: config-service
dependency	: Web, Config Server
```
2. Discovery Service
```
group		: com.emerio.rnd
artifact	: discovery-service
dependency	: Web, Config Client, *Eureka Server*
```
3. Customer Service (Web, Config Client, Eureka Discovery, JPA, H2, Ribbon)
```
group		: com.emerio.rnd
artifact	: customer-service
dependency	: Web, Config Client, *Eureka Discovery*, JPA, H2, *Ribbon*
```
4. Account Service (Web, Config Client, Eureka Discovery, JPA, H2, Feign, Ribbon)
```
group		: com.emerio.rnd
artifact	: account-service
dependency	: Web, Config Client, *Eureka Discovery*, JPA, H2, *Ribbon*, *Feign*
```
5. Gateway Service (Web, Config Client, Eureka Discovery, Zuul, Ribbon)
```
group		: com.emerio.rnd
artifact	: account-service
dependency	: Web, Config Client, *Eureka Discovery*, JPA, H2, *Ribbon*
```